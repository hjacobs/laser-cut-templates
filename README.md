# Laser Cut Templates

Some vector files (SVG) for laser cutting

* `bird-food-box`: a plywood box to feed birds
* `code-dial`: script to generate a code / password dial
* `hersheyfonts`: script to plot characters of the hershey stroke font
* `visual-cryptography`: various scripts to experiment with visual cryptography
