# Visual Cryptography

Generate visual cryptography cards to laser cut.

Usage:

```
poetry install
poetry run ./render.py --text=MyText out.svg
```

To render all glyphs of the 3x3 font:

```
poetry run ./render.py test.svg --only-data-bits --font 3x3.bdf --width=32 --height=44 --space-x=0.6 --space-y=0.6 --card-width=150 --card-height=200 --text '"'"'"'+,-./0123456789:<=>?ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdfhijlmnoruv{}~' --skip-rows=4 --skip-cols=4 --shape=3dots
```

## Fonts

* `3x3.bdf`: very small 3x3 pixel font, not all characters are available (best used with `--skip-cols` and `--skip-rows` set to 4)
* `tom-thumb.bdf`: small 5x7 pixel font, all ASCII characters available  (best used with `--skip-cols=4` and `--skip-rows=6`)
* `bitocra.bdf`: large OCR font

## Examples

Generate cards with the letters A-K on two rows using the default font `tom-thumb.bdf`:

```
poetry run ./render.py out.svg --skip-cols=4 --skip-rows=6 --text=ABCDEFGHIJK --space-x=0.6 --space-y=0.6
```
