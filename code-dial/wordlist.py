#!/usr/bin/env python3
"""
Render all 2048 mnemonic words on one A4 page.
"""

import argparse
import secrets
import string
import svgwrite
import math
from HersheyFonts import HersheyFonts

STROKE_WIDTH = 0.2

parser = argparse.ArgumentParser()
parser.add_argument("output")
parser.add_argument("--stroke-width", type=float, default=STROKE_WIDTH)
parser.add_argument("--bottom", action="store_true")

args = parser.parse_args()

STROKE_WIDTH = args.stroke_width

dwg = svgwrite.Drawing(
    args.output, ("210mm", "297mm"), profile="tiny", viewBox="0 0 210 297"
)

import mnemonic

thefont = HersheyFonts()
thefont.load_default_font("futural")

m = mnemonic.Mnemonic("english")
for i, word in enumerate(m.wordlist):
    thefont.normalize_rendering(2.4)
    strokes = list(thefont.strokes_for_text(word))
    minp = [math.inf, math.inf]
    maxp = [-math.inf, -math.inf]
    for stroke in strokes:
        for d in range(2):
            for p in stroke:
                minp[d] = min(minp[d], p[d])
                maxp[d] = max(maxp[d], p[d])
    centerx = (minp[0] + maxp[0]) / 2
    centery = (minp[1] + maxp[1]) / 2
    x = i // 114
    y = i % 114
    for stroke in strokes:
        for idx, point in enumerate(stroke):
            stroke[idx] = (
                10 + (x * 11.2) + point[0] - centerx,
                3 + (y * 2.55) - point[1] + centery,
            )

        pl = dwg.polyline(
            stroke, stroke="black", stroke_width=STROKE_WIDTH, fill="none"
        )
        dwg.add(pl)

dwg.save()
