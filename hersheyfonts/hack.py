#!/usr/bin/env python3

import svgwrite
from svgwrite import cm, mm


dwg = svgwrite.Drawing("test.svg", ("210mm", "297mm"), profile="tiny", viewBox="0 0 210 297")
l = dwg.path("M 1 1 10 10", stroke="black", stroke_width=0.2)
dwg.add(l)



# Minimalistic code for easy start
from HersheyFonts import HersheyFonts
def draw_line(x1, y1, x2, y2):
    l = dwg.line((x1, -y1), (x2, -y2), stroke="green", stroke_width=0.2)
    dwg.add(l)

def draw_all():

    for i, font_name in enumerate(HersheyFonts().default_font_names):

        thefont = HersheyFonts()
        thefont.load_default_font(font_name)
        thefont.normalize_rendering(10)
        all_glyphs_text = ''.join(sorted(thefont.all_glyphs.keys()))
        for stroke in thefont.strokes_for_text(f"{i} {font_name}: {all_glyphs_text}"):
            for j, point in enumerate(stroke):
                stroke[j] = (point[0], -point[1] + (i*10))

            pl = dwg.polyline(stroke, stroke="green", stroke_width=0.2, fill="none")
            dwg.add(pl)

            #draw_line(x1, y1 + (10 * i) ,x2 ,y2 + (10 * i))

for i, font_name in enumerate(HersheyFonts().default_font_names):
    print(font_name)

thefont = HersheyFonts()
thefont.load_default_font("futural")
thefont.normalize_rendering(5)
all_glyphs_text = ''.join(sorted(thefont.all_glyphs.keys()))
for stroke in thefont.strokes_for_text(f"{all_glyphs_text}"):
    for j, point in enumerate(stroke):
        stroke[j] = (point[0], -point[1])

    pl = dwg.polyline(stroke, stroke="green", stroke_width=0.2, fill="none")
    dwg.add(pl)

dwg.save()

